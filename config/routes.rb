Rails.application.routes.draw do
  devise_for :users, path: 'auth', path_names: { sign_in: 'login', sign_out: 'logout', password: 'secret', confirmation: 'verification', unlock: 'unblock', registration: 'register', sign_up: 'cmon_let_me_in' }
  get 'landing/index'
  resources :articles
  scope :articles do
  	post 'like' => 'articles#like'
  	post 'comment' => 'articles#comment'
  	post '/:article_id/comments' => 'articles#save_comments'
  	get '/:article_id/comments' => 'articles#comments'
    post '/:article_id/publish' => 'articles#publish'
  end

  scope :dashboard do
    get '/my_articles' => 'dashboard#my_articles'
  end
  root 'landing#index'
end
