class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string :name
      t.string :link
      t.text :description
      t.belongs_to :user, index: true, foreign_key: true
      t.integer :status
      t.timestamps
    end
  end
end
