FROM ruby:2.6.1-alpine

RUN apk add --no-cache --update build-base linux-headers git postgresql-dev nodejs tzdata imagemagick yarn curl

WORKDIR /readme
ADD Gemfile Gemfile.lock /readme/
RUN bundle install

ADD . .
RUN yarn install
RUN bundle exec rails assets:precompile