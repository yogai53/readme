import React from "react"
import PropTypes from "prop-types"
import Menu from "./common/menu"
import MyArticleList from "./common/article/list"
class MyArticle extends React.Component {
  render () {
    return (
      <React.Fragment>
      	<MyArticleList list={this.props.list} user={this.props.currentUser}/>
      	{
      		this.props.currentUser && <Menu user={this.props.currentUser}/>
      	}
      </React.Fragment>
    );
  }
}


export default MyArticle
