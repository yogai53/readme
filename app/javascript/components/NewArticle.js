import React from "react"
import PropTypes from "prop-types"
import Menu from "./common/menu"
import ArticleForm from "./common/article/form"
class NewArticle extends React.Component {
  render () {
    return (
      <React.Fragment>
      	{
      		this.props.currentUser && <Menu user={this.props.currentUser}/>
      	}
      	<ArticleForm type='new'/>
      </React.Fragment>
    );
  }
}


export default NewArticle
