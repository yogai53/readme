import React from "react"
import PropTypes from "prop-types"
import Menu from "./common/menu"
import Article from "./common/article"
class Landing extends React.Component {
  render () {
    return (
      <React.Fragment>
      	<Article user={this.props.currentUser}/>
      	{
      		this.props.currentUser && <Menu user={this.props.currentUser}/>
      	}
      </React.Fragment>
    );
  }
}


export default Landing
