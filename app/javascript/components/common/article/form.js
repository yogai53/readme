import React from "react"
import PropTypes from "prop-types"
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
class Form extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			canValidate: false, 
			error: {
				name: '',
				link: '',
				description: ''
			},
			form: {
				name: props.article ? props.article.name : '',
				link: props.article ? props.article.link : '',
				description: props.article ? props.article.description : '',
				publish: props.article ? props.article.status == 'published' : false
			},
			serverError: ''
		}
	}

	handleInputChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
    	form: {...this.state.form, ...{[name]: value}}
    });
    if(this.state.canValidate) this.validate({...this.state.form, ...{[name]: value}})
  }

  is_url = (str) => {
  	const regexp =  /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
  	if (regexp.test(str))
  	{
  		return true;
  	}
  	else
  	{
  		return false;
  	}
  }

  validate = (form) => {
  	let error = {
  		name: '',
			link: '',
			description: ''
  	}
  	//Name Validation
  	const name = form.name
    const link = form.link
    const description = form.description

  	if(name.length <5) error.name = 'Name Too Short. Enter atleast 5 characters.'
  	if(name.length >50) error.name = 'Name Too Long. Enter within 50 characters.'
  	if(name.length == 0) error.name = 'Name Empty.'

  	//Link Validation
    if(description.length > 0){
      if(link.length > 0){
        if(!this.is_url(link)) error.link = 'Please include http.'
      }
    }
    else{
      if(link.length == 0) error.link = 'Link Empty.'
      if(!this.is_url(link)) error.link = 'Please include http.'
    }
  	
    //Description Validation
  	if(link.length > 0){
      if(description.length > 0){
        if(description.length <20) error.description = 'Description Too Short. Enter atleast 30 characters.'
        if(description.length >300) error.description = 'Description Too Long. Enter within 300 characters.'
      }
    }
    else{
      if(description.length == 0) error.description = 'Description Empty.'
      if(description.length <20) error.description = 'Description Too Short. Enter atleast 30 characters.'
      if(description.length >1000) error.description = 'Description Too Long. Enter within 300 characters.'
    }
  	
  	this.setState({error: error})
  	return error.name == '' && error.link == '' && error.description == '' 
  }

  handleSubmit = (event) => {
  	event.preventDefault();
  	this.setState({canValidate: true})
  	if(!this.validate(this.state.form)) return
  	
  	let url = ''
  	if(this.props.type == 'new') url = '/articles'
  	else if(this.props.type == 'edit') url = `/articles/${this.props.article.id}`

  	const csrfToken = document.querySelector("meta[name=csrf-token]").content
  	axios.defaults.headers.common['X-CSRF-Token'] = csrfToken

    if(this.props.type == 'new'){
      axios.post(url, this.state.form)
      .then(r => {
        window.location = '/'
      })
      .catch(e => {
        console.log(e)
        this.setState({serverError: 'Problem in saving. Please try again'})
      })
    }
    else
    {
       axios.put(url, this.state.form)
      .then(r => {
        window.location = '/'
      })
      .catch(e => {
        console.log(e)
        this.setState({serverError: 'Problem in saving. Please try again'})
      })
    }
  	
  }

  hideAlert = () => {
  	this.setState({serverError: ''})
  }

  _renderFormWarning = () => (
  	<SweetAlert warning title="Error" onConfirm={this.hideAlert}>
	    {this.state.serverError}
		</SweetAlert>
  )
  
	render () {
		return (
				<div className="container">
					<div className="row">
						<div className="col-md-8 bg-light mt-5">
							<form>
                <h4 className='mt-5 pt-5 mb-5'>ReadMe</h4> 
								<div className="form-group">
									<label>
										{this.props.type=='new' && <h5>Welcome Back. Time to Create a new Article. </h5>}
									</label>
								</div>
								<div className="form-group">
									<label htmlFor="name">Name</label>
									<input type="text" value={this.state.form.name} onChange={this.handleInputChange} className="form-control" name="name" id="name" placeholder="Article Name" />
									<span className="text-danger">{this.state.error.name}</span>	
								</div>
								<div className="form-group">
									<label htmlFor="name">Link</label>
									<input type="text" value={this.state.form.link}  onChange={this.handleInputChange} className="form-control" name="link" id="link" placeholder="Article Link" />
									<span className="text-danger">{this.state.error.link}</span>	
								</div>
								<div className="form-group">
									<label htmlFor="description">Description</label>
									<textarea className="form-control" name="description" id="description" rows="3" value={this.state.form.description} onChange={this.handleInputChange} />
									<span className="text-danger">{this.state.error.description}</span>	
								</div>
								<div className="form-check">
									<input className="form-check-input" type="checkbox" value="" name="publish" id="publish" checked={this.state.form.publish} onChange={this.handleInputChange} />
									<label className="form-check-label" htmlFor="publish">
										Publish
									</label>
								</div>
								<button type="submit" className="btn btn-primary mb-2 mt-5 btn-block" onClick={this.handleSubmit}>Submit</button>
								{this.state.serverError != '' && this._renderFormWarning()}
							</form>
						</div>
					</div>
				</div>
			);
	}
}

export default Form
