import React from "react"
import PropTypes from "prop-types"
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
class List extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			serverError: '',
			serverSuccess: '',
			articles: this.props.list
		}
	}

	handlePublishClick = (articleId) => {
		const csrfToken = document.querySelector("meta[name=csrf-token]").content
  		axios.defaults.headers.common['X-CSRF-Token'] = csrfToken
		axios.post(`/articles/${articleId}/publish`)
			.then(r => {
				if(r.data.success == true)
				{
					const articles = this.state.articles.slice()
					let article = articles.filter(a => a.id == articleId)[0]
					article.status = 'published'
					this.setState({articles: articles, serverSuccess: 'Published'})
				}
				else
				{
					this.setState({serverSuccess: 'Could not publish at this moment.'})
				}
			})
			.catch(e => {
				this.setState({serverSuccess: 'Could not publish at this moment.'})
			})
	}

	handleEditClick = (articleId) => {
		window.location = `/articles/${articleId}/edit`
	}

	hideAlert = () => {
		this.setState({serverError: '', serverSuccess: ''})
	}

	_renderFormWarning = () => (
		<SweetAlert warning title="Error" onConfirm={this.hideAlert}>
			{this.state.serverError}
		</SweetAlert>
	)

	_renderFormSuccess = () => (
		<SweetAlert warning title="Success" onConfirm={this.hideAlert}>
			{this.state.serverError}
		</SweetAlert>
	)

	_renderArticleList = () => {
		if(this.state.articles.length == 0)
		{
			return (
				<tr>
					<td colSpan='4'>
						<h4 className="text-center">No Records found</h4>
					</td>
				</tr>
			)
		}
		else
		{
			return this.state.articles.map((l, i) => (
				  			<tr key={l.id}>
						      <th scope="row">{i+1}</th>
						      <td>{l.name}</td>
						      <td>{l.status}</td>
						      <td>
						      	{ l.status=='unpublished' && <button type="button" className="btn btn-info" onClick={() => this.handlePublishClick(l.id)}>Publish</button>}
						      	<button type="button" className="btn btn-info ml-2" onClick={() => this.handleEditClick(l.id)}>Edit</button>
						      </td>
						    </tr>
				  		))
		}
	}

	render(){
		return(
			<div className='container-fluid'>
				<div className='row'>
					<div className={this.props.user ? 'col-md-10 mt-50' : 'col-md-10 offset-md-1 mt-50'}>
						<div className='sticky'>
							<h3>Read Me</h3>
						</div>
						<table className="table table-dark">
						  <thead>
						    <tr>
						      <th scope="col">#</th>
						      <th scope="col">Name</th>
						      <th scope="col">Status</th>
						      <th scope="col">Action</th>
						    </tr>
						  </thead>
						  <tbody>
						  	{this._renderArticleList()}
						  </tbody>
						</table>
						{this.state.serverError && this._renderFormWarning()}
						{this.state.serverSuccess && this._renderFormSuccess()}
					</div>
				</div>
			</div>
		)
	}
}

export default List