import React from "react"
import axios from 'axios'
class Menu extends React.Component {
	openNav = () => {
		document.getElementById("mySidenav").style.width = "250px";
	}

	closeNav = () => {
		document.getElementById("mySidenav").style.width = "0";
	}

	componentDidMount(){
		setTimeout(() => this.openNav(), 300);
	}


	logout = (e) => {
		e.preventDefault();
		const csrfToken = document.querySelector("meta[name=csrf-token]").content
		axios.defaults.headers.common['X-CSRF-Token'] = csrfToken
		axios.delete('/auth/logout').then(r => {
			window.location = '/'
		}).catch(e => {
			window.location = '/'
		})
	}

	render () {
		return (
			<React.Fragment>
				<span style={{ position: 'fixed', right: '0', top: '0', fontSize: '50px', cursor: 'pointer', float: 'right', marginRight: '30px', marginTop: '10px'}} onClick={this.openNav}>&#9776;</span>
				<div id="mySidenav" className="sidenav">
					<a href="javascript:void(0)" className="closebtn" onClick={this.closeNav} style={{ fontSize: '50px'}}>&times;</a>
					<a href="/">Home</a>
					<a href="/articles/new/">Create Article</a>
					<a href="/dashboard/my_articles">Your Articles</a>
					<a href="#" onClick={this.logout}>Logout</a>
				</div>
			</React.Fragment>
			);
	}
}

export default Menu
