import React from "react"
import axios from 'axios'
import SweetAlert from 'react-bootstrap-sweetalert'
import InfiniteScroll from 'react-infinite-scroll-component';
import Comments from './comments'

class Article extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			articles: [],
			serverError: '',
			offset: 0, 
			limit: 10,
			showNavigateTopbutton: false,
			hasMore: true,
			isHover: [],
			activeComments: [],
			comments: [],
			inputComment: '',
			commentsError: '',
			activeArticleId: null
		}
	}

	componentDidMount(){
		this.fetchArticles()
  		$('[data-toggle="tooltip"]').tooltip();
	}

	fetchArticles = () => {
		axios.get('/articles', {params: {offset: this.state.offset, limit: this.state.limit}})
			.then(r => {
				let isHover = []
				this.setState({activeComments: Array(r.data.articles.length).fill(false), isHover: Array(r.data.articles.length).fill(false), hasMore: r.data.articles.length > 0,articles: [...this.state.articles, ...r.data.articles], offset: this.state.offset + this.state.limit})
			})
			.catch(e => {
				console.log(e)
			})
	}

	handleClickArticle = (i, article) => {
		if(article.link == ''){
			this.retrieveComments(i, article)
		}
		else
		{
			window.open(article.link)
		}
	}

	handleMouseEnterButton = (index) => {
		let isHover = Array(this.state.articles.length).fill(false)
		isHover[index] = true;
		this.setState({isHover})
	}

	handleMouseLeaveButton = () => {
		let isHover = Array(this.state.articles.length).fill(false)
		this.setState({isHover})
	}

	handleLikeButtonClick = (i, article, e) => {
		e.stopPropagation();
		if(this.props.user){
			const csrfToken = document.querySelector("meta[name=csrf-token]").content
  			axios.defaults.headers.common['X-CSRF-Token'] = csrfToken
			axios.post('/articles/like', {article_id: article.id})
			.then(r => {
				if(r.data.success){
					let articles = this.state.articles.slice()
					let selected_article = articles.filter(a => a.id == article.id)[0]
					selected_article.liked = true
					selected_article.likes_count++
					this.setState({articles: articles})
				}
				else
				{
					
				}
			})
			.catch(e => {
				console.log(e)
			})
		}
		else
		{
			window.location = '/auth/login'
		}
	}

	retrieveComments = (i, article) => {
		axios.get(`/articles/${article.id}/comments`)
			.then(r => {
				let activeComments = Array(this.state.articles.length).fill(false)
				activeComments[i] = true;
				this.setState({comments: r.data.comments, activeComments: activeComments, commentsError: '', activeArticleId: article.id})
			})
			.catch(e => {
				console.log(e)
			})
	}

	handleCommentButtonClick = (i, article, e) => {
		e.stopPropagation();
		this.retrieveComments(i, article)
	}

	handleCommentChange = (e) => {
		const comments = e.target.value
		this.setState({inputComment: e.target.value})
	}

	handleClickCommentSubmit = (e) => {
		if(this.state.inputComment.length < 10)
		{
			this.setState({commentsError: 'Please enter atleast 10 characters'})
			return
		}
		else if(this.state.inputComment.length > 500)
		{
			this.setState({commentsError: 'Please enter not more than 500 characters'})
			return
		}
		const articleId = 1
	
		const csrfToken = document.querySelector("meta[name=csrf-token]").content
		axios.defaults.headers.common['X-CSRF-Token'] = csrfToken
		axios.post(`/articles/${this.state.activeArticleId}/comments`, {comments: this.state.inputComment})
		.then(r => {
			this.setState({comments: r.data.comments, commentsError: '', inputComment: ''})
		})
		.catch(e => {
			console.log(e)
		})
	}

	_renderArticleComments = () => (<div className="article-comments">
		<div className="col-md-8">
			{
				this.props.user && <React.Fragment>
					<div className="form-group m-3">
					    <label htmlFor="comments">Enter your comments</label>
					    <textarea className="form-control" id="comments" rows="3" onChange={this.handleCommentChange} value={this.state.inputComment} />
						<span className='text-danger'>{this.state.commentsError}</span>
					</div>
					<button type="button" className="btn btn-info m-3" onClick={this.handleClickCommentSubmit}>Submit</button>
				</React.Fragment>
			}
			
			{ this.state.comments.length > 0 && <div className="list-group">
				{this.state.comments.map(c => (
					<a href="#" key={c.id} className="list-group-item list-group-item-action">
						<div className="d-flex w-100 justify-content-between">
							<b className="mb-1">{c.user.email}</b>
							<small>{c.pretty_date}</small>
						</div>
						<p className="mb-1">{c.message}</p>
					</a>
				))}
			</div>
			}
			{
				this.state.comments.length == 0 && <h4 className="text-center" style={{marginTop: '200px'}}>No Comments Found</h4>
			}
		</div>
	</div>)

	_renderArticles = () => {
		if(this.state.articles.length > 0)
		{
			return this.state.articles.map((a, i) => (
				<React.Fragment key={i}>
		 			<button type="button" className="list-group-item list-group-item-action article-item" onClick={() => this.handleClickArticle(i, a)} onMouseEnter={() => this.handleMouseEnterButton(i)} onMouseLeave={() => this.handleMouseLeaveButton()}>
					    <div className="row">
					    	<div className="col-md-8">
					    		<h5 className="float-left">{a.name}</h5>
					    	</div>

					    	{ this.state.isHover[i]==true && <div className="col-md-4">
					    		<div className="btn btn-outline-dark float-right mr-2 border-0" data-toggle="tooltip" data-placement="bottom" title="Comment" onClick={(e) => this.handleCommentButtonClick(i, a, e)}>
					    			<i className="far fa-comment-alt"></i>
					    		</div>
					    		{
					    			a.liked == true && <i data-toggle="tooltip" title="You liked already" className="fas fa-thumbs-up text-primary float-right p-2"></i>
					    		}
					    		{
					    			a.liked == false && <div className="btn btn-outline-dark float-right border-0" data-toggle="tooltip" data-placement="bottom" title="Like" onClick={(e) => this.handleLikeButtonClick(i, a, e)}>
								    			<i className="fas fa-thumbs-up"></i>
								    		</div>
					    		}
					    	</div> }
					    	{ this.state.isHover[i]==false && <div className="col-md-4">
					    		<span className="float-right"> {a.comments.length} Comments</span>
					    		<span className="float-right">{a.likes_count} likes | </span>
					    	</div> }
					    </div>
					    <small>{a.pretty_date}</small>	
					    <p>{a.description}</p>	
				  	</button>
	    			{ this.state.activeComments[i]==true && this._renderArticleComments() }
			  	</React.Fragment>
			))
		}
		else
		{
			return (
				<React.Fragment>
					<div className="alert alert-light text-center" role="alert">
					  <h3>No News For Today. Want to be the first person to Add?</h3>
					</div>
					<div className="btn btn-secondary" onClick={() => window.location = '/articles/new'}>Add Article</div>
				</React.Fragment>
			);
		}
	}

	loadFunc = () => {
		this.setState({showNavigateTopbutton: true})
		this.fetchArticles()
	}

	refresh =() => {
		console.log('asdad')
	}

	_renderFormWarning = () => (
		<SweetAlert warning title="Error" onConfirm={this.hideAlert}>
			{this.state.serverError}
		</SweetAlert>
	)

	_loader =
		<button type="button" className="list-group-item list-group-item-action">
		    <h4>Loading...</h4>
	  	</button>

	goUp = (scrollDuration) => {
		let scrollStep = -window.scrollY / (scrollDuration / 15),
		scrollInterval = setInterval(() => {
			if ( window.scrollY != 0 ) {
				window.scrollBy( 0, scrollStep );
			}
			else {
				this.setState({showNavigateTopbutton: false})
				clearInterval(scrollInterval); 
			}
		},15);
	}

	render () {
		return (
			<div className='container-fluid'>
				<div className='row'>
					<div className={this.props.user ? 'col-md-10 mt-50' : 'col-md-10 offset-md-1 mt-50'}>
						<div className='sticky'>
							<h3 style={{display: 'inline'}}>Read Me</h3>
							<a href="/auth/login" className='btn btn-info float-right'>Login</a>
						</div>
						<InfiniteScroll
							dataLength={this.state.articles.length} //This is important field to render the next data
							next={this.loadFunc}
							hasMore={this.state.hasMore}
							loader={this._loader}
							className='list-group list-group-flush'
						>
							{this._renderArticles()}
						</InfiniteScroll>
						{this.state.serverError != '' && this._renderFormWarning()}

						{ this.state.showNavigateTopbutton && <button className="position-fixed up-button" style={{left: '100px'}} onClick={() => this.goUp(10000)}>Up</button> }
					</div>
				</div>
			</div>
			);
	}
}

export default Article
