class ArticlesController < ApplicationController
	before_action :authenticate_user!, except: [:index, :comments]
	def index
		articles = Article.where(status: 'published').order(created_at: :desc)
							.offset(params[:offset])
							.limit(params[:limit])
							.all
							.as_json({ 
								include: {
									comments: {
										include: [:user]
									}
								},
								 methods:  [
									 	:likes_count, 
									 	:pretty_date, 
									 	:created_at_date
									] 
								})

		articles.each do |a|
			a[:liked] = Like.where(likeable_type: 'Article')
							.where(likeable_id: a['id'])
							.where(user: current_user)
							.first.present?

		end
		
		return render json: {
			articles: articles
		}
	end

	def new
	end

	def create
		@article = current_user.articles.create({name: params[:name], link: params[:link], description: params[:description], status: params[:publish] ? 'published' : 'unpublished' })
		return render json: {success: true, article: @article}
	end

	def like
		@article = Article.find(params['article_id'])
		liked = @article.likes.where(user: current_user).first
		if liked
			return render json: {success: false}
		else
			@article.likes.build(user: current_user).save
			return render json: {success: true}
		end
	end

	def save_comments
		@article = Article.find(params[:article_id])
		@article.comments.create(message: params[:comments], user: current_user)

		return render json: {success: true, comments: Article.find(params[:article_id]).comments.as_json({include: [:user], methods: [:pretty_date]})}
	end

	def comments
		return render json: {comments: Article.find(params[:article_id]).comments.as_json({include: [:user], methods: [:pretty_date]})}
	end

	def publish
		current_user.articles.find(params[:article_id]).update(status: 'published')
		return render json: {success: true}
	end

	def edit
		@article = current_user.articles.find(params[:id])
	end

	def update
		article = current_user.articles.find(params[:id])
		article.update({name: params[:name], link: params[:link], description: params[:description], status: params[:publish] ? 'published' : 'unpublished' })
		return render json: {success: true}
	end
end

