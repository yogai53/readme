class DashboardController < ApplicationController
	before_action :authenticate_user!
	def my_articles
		@articles = current_user.articles.all
	end
end
