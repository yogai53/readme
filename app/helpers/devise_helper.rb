module DeviseHelper

  def devise_error_messages!
  	devise_error_messages
  	render "devise/shared/error_messages", resource: resource
     flash[:alert] = resource.errors.full_messages.first
     flash[:notice] = resource.errors.full_messages.first
  end
end