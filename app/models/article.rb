class Article < ApplicationRecord
	enum status: [:unpublished, :published, :blocked]

	has_many :comments, :as => :commentable
	has_many :likes, :as => :likeable
	belongs_to :user
	has_one_attached :image
	attr_accessor :created_at_date, :pretty_date

	def created_at_date
		self.created_at.strftime('%b %d %Y')
	end

	def pretty_date
		a = Time.now.to_i - self.created_at.to_i
		case a
	      when 0..59 then 'just now'
	      when 60..119 then 'a minute ago'
	      when 120..7100 then 'an hour ago'
	      when 7101..82800 then ((a+99)/3600).to_i.to_s+' hours ago' 
	      when 82801..172000 then 'yesterday'
	      when 172001..518400 then self.created_at.strftime('%A')
	      when 518400..1036800 then self.created_at.strftime('%b %d %Y')
	      else self.created_at.strftime('%b %d %Y')
	    end
	end

	def likes_count
		self.likes.count
	end
end
